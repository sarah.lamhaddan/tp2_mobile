package com.licence.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static com.licence.tp2.WineDbHelper.cursorToWine;

public class MainActivity extends AppCompatActivity {

    private SimpleCursorAdapter wineAdapter;
    private WineDbHelper wineHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView wineList = findViewById(R.id.Listwine);

        wineHelper = new WineDbHelper(this);
        wineHelper.populate();

        wineAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                wineHelper.fetchAllWines(),
                new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);
        wineList.setAdapter(wineAdapter);

        wineList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine", cursorToWine((Cursor) parent.getItemAtPosition(position)));
                intent.putExtra("reWine", 1);
                startActivityForResult(intent, 1);
            }
        });

        //Bouton ajout
        final FloatingActionButton addActionButton = findViewById(R.id.addWine_btn);
        addActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Wine newWine = new Wine(null, null, null, null, null);
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine", newWine);
                intent.putExtra("reWine", 2);
                startActivityForResult(intent, 2);
            }
        });

        // Pour afficher le bouton delete
        wineList.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                AdapterView.AdapterContextMenuInfo del = (AdapterView.AdapterContextMenuInfo) menuInfo;
                menu.add(0, 0, 0, "Supprimer");
            }
        });

    }

    @Override

    // On clique sur un item pour savoir notre position puis ensuite on delete un vin.

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        AdapterView adapter = findViewById(R.id.Listwine);
        Cursor target = (Cursor) adapter.getItemAtPosition(info.position);
        WineDbHelper vin = new WineDbHelper(MainActivity.this);

        vin.deleteWine(target);
        Intent delIntent = new Intent(MainActivity.this, MainActivity.class);
        startActivity(delIntent);
        Toast.makeText(MainActivity.this, "Le vin '" + cursorToWine(target).getTitle() + "' va être supprimé", Toast.LENGTH_LONG).show();
        finish();
        return true;
    }

    @Override

    // Si on modifie ca va faire mise a jour de la liste a chaque fois que si on a changé supp ou ajouter un item
    protected void onActivityResult(int requestCode, int resultCode, @Nullable  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        wineAdapter.changeCursor(wineHelper.fetchAllWines());
        wineAdapter.notifyDataSetChanged();
    }
}